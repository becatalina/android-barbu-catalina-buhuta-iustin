package com.example.tema3

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainFragment:Fragment() {
    companion object{
        fun newInstance()=MainFragment()
    }

    private lateinit var alarmManager: AlarmManager
    private lateinit var pendingIntent: PendingIntent

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cal=Calendar.getInstance()
        cal.set(Calendar.HOUR,5);
        tv_clock.text=SimpleDateFormat("HH:mm:ss").format(cal.time)
        tv_date.text=SimpleDateFormat("dd/MM/yyyy").format(cal.time)

        createNotificationChannel()

        var year = cal.get(Calendar.YEAR)
        var month = cal.get(Calendar.MONTH)
        var day = cal.get(Calendar.DAY_OF_MONTH)

        btn_clock.setOnClickListener {

            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY,hour)
                cal.set(Calendar.MINUTE,minute)
                cal.set(Calendar.SECOND,0)
                tv_clock.text=SimpleDateFormat("HH:mm:ss").format(cal.time)
            }
            TimePickerDialog(context,timeSetListener,cal.get(Calendar.HOUR_OF_DAY),cal.get(Calendar.MINUTE),true).show()
        }

        btn_date.setOnClickListener {
            val dpd = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { view, year1, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR,year1)
                cal.set(Calendar.MONTH,monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH,dayOfMonth)
                tv_date.text = SimpleDateFormat("dd/MM/yyyy").format(cal.time)
                var year = cal.get(Calendar.YEAR)
                var month = cal.get(Calendar.MONTH)
                var day = cal.get(Calendar.DAY_OF_MONTH)
            }, year, month, day)
            dpd.show()
        }

        btn_create.setOnClickListener {
            Toast.makeText(context,"Notification set!",Toast.LENGTH_SHORT).show()
            alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, MyAlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(context,0,intent,0)


            alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                cal.timeInMillis,
                pendingIntent
            )

        }

    }

    private fun createNotificationChannel(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            var name = "ReminderChannel"
            var description = "Channel for reminder"
            var importance=NotificationManager.IMPORTANCE_DEFAULT
            var channel=NotificationChannel("notify",name,importance)
            channel.description=description

            var notificationManager= context?.getSystemService(NotificationManager::class.java) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}