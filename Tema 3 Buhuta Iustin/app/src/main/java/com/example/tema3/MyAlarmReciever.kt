package com.example.tema3

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

class MyAlarmReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        var builder = NotificationCompat.Builder(context, "notify")
            .setSmallIcon(R.drawable.shape_button_selected)
            .setContentTitle("Notificare")
            .setContentText("NOTIFICARE!!!!")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        var notificationManager=NotificationManagerCompat.from(context)

        notificationManager.notify(200,builder.build())
    }
}