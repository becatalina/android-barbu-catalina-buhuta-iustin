package com.example.tema2v2.Activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tema2v2.Fragments.MainFragment
import com.example.tema2v2.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(savedInstanceState==null){
            supportFragmentManager.beginTransaction()
                .replace(
                    R.id.container,
                    MainFragment.newInstance()
                )
                .commitNow()
        }
    }
}
