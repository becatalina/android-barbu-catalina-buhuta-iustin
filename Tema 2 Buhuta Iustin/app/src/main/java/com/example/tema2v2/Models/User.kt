package com.example.tema2v2.Models

import io.realm.RealmObject

open class User() : RealmObject(){
    var firstName:String = ""
        get() = field
        set(value){
            field = value
        }
    var lastName:String = ""
        get() = field
        set(value){
            field = value
        }
    constructor(firstName:String,lastName:String):this(){
        this.firstName=firstName
        this.lastName=lastName
    }
    fun getName(): String?{
        return "$firstName $lastName"
    }
}