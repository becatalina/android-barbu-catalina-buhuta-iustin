package com.example.tema2v2.Fragments

import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tema2v2.Models.JsonUser
import com.example.tema2v2.Models.User
import com.example.tema2v2.R
import com.example.tema2v2.UsersAdapter
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults
import kotlinx.android.synthetic.main.fragment_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL

class MainFragment:Fragment() {
    companion object {
        fun newInstance() = MainFragment()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    lateinit var users: ArrayList<User>
    lateinit var realm:Realm

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Realm.init(context!!)
        val config = RealmConfiguration.Builder().name("myrealm.realm").build()
        Realm.setDefaultConfiguration(config)
        realm= Realm.getDefaultInstance()
        displayUsers()

        btn_add.setOnClickListener {
            val user = User(
                et_first.text.toString(),
                et_last.text.toString()
            )
            if(checkDublication(user)!=null)
            {
                val toast = Toast.makeText(context!!,"User already exist!", Toast.LENGTH_SHORT)
                toast.show()
                return@setOnClickListener
            }
            users.add(user)
            realm.executeTransaction{ realm -> val u: User = realm.copyToRealm(user)}
            et_first.setText("")
            et_last.setText("")
            displayUsers()
        }
        btn_remove.setOnClickListener {
            val user = User(
                et_first.text.toString(),
                et_last.text.toString()
            )
            if(checkDublication(user)==null)
            {
                val toast = Toast.makeText(context!!,"User doesn't exist!", Toast.LENGTH_SHORT)
                toast.show()
                return@setOnClickListener
            }
            val name=et_first.text.toString()+ " "+et_last.text.toString()
            realm.executeTransaction{searchUser(name)?.deleteFromRealm()}
            et_first.setText("")
            et_last.setText("")
            displayUsers()
        }
        btn_sync.setOnClickListener {
            doAsync {
                val json = URL("https://jsonplaceholder.typicode.com/users").readText()

                uiThread {
                    d("tag","json: $json")
                    val jsonUser = Gson().fromJson(json, Array<JsonUser>::class.java).toList()
                    for (user in jsonUser) {
                        var name = user.name.split(' ')
                        var newUser =
                            User(name[0], name[1])
                        if(checkDublication(newUser)==null)
                            realm.executeTransaction { realm ->
                                val u: User = realm.copyToRealm(newUser)
                            }
                    }
                    displayUsers()
                }

            }

        }
    }
    fun checkDublication(_user : User): User?{
        for(user in users)
        {
            if(user.firstName==_user.firstName && user.lastName==_user.lastName)
            {
                return user
            }
        }
        return null
    }
    fun displayUsers(){
        users = ArrayList()

        var _users: RealmResults<User> = realm.where<User>(
            User::class.java).findAll()
        for(user in _users)
            users.add(user)

        var adapter= UsersAdapter(users)
        rv_users.adapter=adapter
        rv_users.layoutManager= LinearLayoutManager(context!!)
    }
    fun searchUser(name : String): User?{
        var _users: RealmResults<User> = realm.where<User>(
            User::class.java).findAll()
        for(user in _users)
        {
            if(user.getName().equals(name))
                return user
        }
        return null
    }
}
