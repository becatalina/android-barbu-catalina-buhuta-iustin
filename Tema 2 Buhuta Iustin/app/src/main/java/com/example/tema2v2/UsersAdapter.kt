package com.example.tema2v2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tema2v2.Models.User

class UsersAdapter (private val mUsers:List<User>): RecyclerView.Adapter<UsersAdapter.ViewHolder>(){
    inner class ViewHolder(listItemView: View):RecyclerView.ViewHolder(listItemView){
        val firstNameTextView=itemView.findViewById<TextView>(R.id.tv_first_name)
        val lastNameTextView=itemView.findViewById<TextView>(R.id.tv_last_name)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdapter.ViewHolder {
        val context=parent.context
        val inflater= LayoutInflater.from(context)
        val userView=inflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(userView)
    }

    override fun onBindViewHolder(viewHolder: UsersAdapter.ViewHolder, position: Int) {
        val user: User =mUsers.get(position)
        val firstTextView = viewHolder.firstNameTextView
        firstTextView.setText(user.firstName)
        val lastTextView=viewHolder.lastNameTextView
        lastTextView.setText(user.lastName)

    }

    override fun getItemCount(): Int {
        return mUsers.size
    }
}