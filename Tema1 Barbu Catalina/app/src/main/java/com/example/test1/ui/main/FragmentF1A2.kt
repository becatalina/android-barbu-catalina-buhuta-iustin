package com.example.test1.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.test1.R
import com.example.test1.SecondActivity
import kotlinx.android.synthetic.main.f1a2.*

class FragmentF1A2: Fragment() {


    companion object {
        fun newInstance() = FragmentF1A2()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.f1a2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnLaunchF2A2.setOnClickListener(){
            (activity as SecondActivity)
                .replaceFragment(SecondActivity.Companion.FragmentTag.TAG_FRAGMENT_F2A2)
        }

    }



}