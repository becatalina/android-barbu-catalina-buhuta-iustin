package com.example.test1

import android.content.Intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.test1.ui.main.MainFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
        findViewById<Button>(R.id.btnOpenActivity2)?.setOnClickListener{
            val intent = Intent(this, SecondActivity::class.java)
            this.startActivity(intent)
        }



    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }




}
