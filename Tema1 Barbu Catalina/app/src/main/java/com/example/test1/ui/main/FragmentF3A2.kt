package com.example.test1.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

import com.example.test1.R
import kotlinx.android.synthetic.main.f2a2.*

class FragmentF3A2: Fragment() {
    companion object{
        fun newInstance()=FragmentF3A2()
    }
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.f3a2, container, false)
    }




}