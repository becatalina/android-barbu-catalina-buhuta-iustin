package com.example.test1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.test1.ui.main.FragmentF1A2
import com.example.test1.ui.main.FragmentF2A2
import com.example.test1.ui.main.FragmentF3A2


class SecondActivity : AppCompatActivity() {
    companion object{
        enum class FragmentTag(val value: String){
            TAG_FRAGMENT_F3A2 ("TAG_FRAGMENT_F3A2"),
            TAG_FRAGMENT_F1A2("TAG_FRAGMENT_F1A2"),
            TAG_FRAGMENT_F2A2 ("TAG_FRAGMENT_F2A2")
        }
    }

    var currentFragmentTag: FragmentTag? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)

        replaceFragment(FragmentTag.TAG_FRAGMENT_F1A2)


    }



    fun replaceFragment(TAG: FragmentTag?) {TAG?.run{
        val fragment = when(this){
            FragmentTag.TAG_FRAGMENT_F1A2 -> FragmentF1A2.newInstance()
            FragmentTag.TAG_FRAGMENT_F2A2 -> FragmentF2A2.newInstance()
            FragmentTag.TAG_FRAGMENT_F3A2 -> FragmentF3A2.newInstance()
        }
        supportFragmentManager.beginTransaction()
            //ps: add never worked for me, I have no idea why
            // You have to open up the up twice in order to test all functionality
            // Sorry. I'll investigate it more
            .replace(R.id.secondContainer, fragment, value) .addToBackStack(TAG.value)
            .commit()

    }}
    fun removeFragment(Tag: FragmentTag?){

        val fragment = supportFragmentManager.findFragmentByTag(Tag?.value)
        if(fragment != null){
            supportFragmentManager.beginTransaction().remove(fragment).commit()
        }
    }




}