package com.example.test1.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.test1.R
import com.example.test1.SecondActivity
import kotlinx.android.synthetic.main.f1a2.*
import kotlinx.android.synthetic.main.f2a2.*


class FragmentF2A2 : Fragment() {
    companion object{
        fun newInstance() = FragmentF2A2()
    }
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        return inflater.inflate(R.layout.f2a2, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        btnRemoveF1A2.setOnClickListener(){
            (activity as SecondActivity)
                .removeFragment(SecondActivity.Companion.FragmentTag.TAG_FRAGMENT_F1A2)
        }
        btnClose.setOnClickListener(){
            this.activity?.finish()
        }
        btnReplace.setOnClickListener(){
            (activity as SecondActivity)
                .replaceFragment(SecondActivity.Companion.FragmentTag.TAG_FRAGMENT_F3A2)
        }
    }



}