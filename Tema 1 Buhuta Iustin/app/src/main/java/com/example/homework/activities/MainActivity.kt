package com.example.homework.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.homework.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_one.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_to_second_act.setOnClickListener{
            goToSecondActivity()
        }
    }

    private fun goToSecondActivity(){
        val intent= Intent(this,SecondActivity::class.java)
        startActivity(intent)
    }
}
