package com.example.homework.activities

import android.nfc.Tag
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.homework.R
import com.example.homework.fragments.FirstFragment
import com.example.homework.fragments.SecondFragment
import com.example.homework.fragments.ThirdFragment
import kotlinx.android.synthetic.main.fragment_one.*
import java.lang.Exception

class SecondActivity: AppCompatActivity(){
    companion object{
        enum class FragmentsTag(val value:String){
            TAG_FIRST_FRAGMENT("TAG_FIRST_FRAGMENT"),
            TAG_SECOND_FRAGMENT("TAG_SECOND_FRAGMENT"),
            TAG_THIRD_FRAGMENT("TAG_THIRD_FRAGMENT")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        replaceFragment(FragmentsTag.TAG_FIRST_FRAGMENT)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
    fun replaceFragment(TAG:FragmentsTag?){
        TAG?.run {
            val fragment =when(this){
                FragmentsTag.TAG_FIRST_FRAGMENT -> FirstFragment.newInstance()
                FragmentsTag.TAG_SECOND_FRAGMENT -> SecondFragment.newInstance()
                FragmentsTag.TAG_THIRD_FRAGMENT -> ThirdFragment.newInstance()
            }

            val transaction=supportFragmentManager.beginTransaction()
            transaction.add(R.id.fly_activity_second,fragment,value).addToBackStack(TAG.value).commit()
        }
    }
    fun removeFragment(TAG: FragmentsTag?){
        val fragment =supportFragmentManager.findFragmentByTag(TAG?.value)
        if(fragment!=null)
            supportFragmentManager.beginTransaction().remove(fragment).commit()
    }
}