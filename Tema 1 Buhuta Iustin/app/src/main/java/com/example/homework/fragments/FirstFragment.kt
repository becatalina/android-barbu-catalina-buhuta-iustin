package com.example.homework.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homework.R
import com.example.homework.activities.SecondActivity
import kotlinx.android.synthetic.main.fragment_one.*

class FirstFragment:Fragment() {
    companion object{
        fun newInstance()=FirstFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_one,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_next.setOnClickListener {
            (activity as SecondActivity).replaceFragment(SecondActivity.Companion.FragmentsTag.TAG_SECOND_FRAGMENT)
        }
    }
}