package com.example.homework.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.homework.R
import com.example.homework.activities.SecondActivity
import kotlinx.android.synthetic.main.fragment_two.*

class SecondFragment:Fragment() {
    companion object{
        fun newInstance()=SecondFragment()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_two,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_remove.setOnClickListener {
            (activity as SecondActivity).removeFragment(SecondActivity.Companion.FragmentsTag.TAG_FIRST_FRAGMENT)
        }
        btn_close.setOnClickListener {
            (activity as SecondActivity).finish()
        }
        btn_replace.setOnClickListener {
            (activity as SecondActivity).replaceFragment(SecondActivity.Companion.FragmentsTag.TAG_THIRD_FRAGMENT)
        }
    }
}