package com.example.todoproj.adapters
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoproj.R
import com.example.todoproj.model.ToDo
import kotlinx.android.synthetic.main.item_todo.view.*

class ToDosAdapter(
    private val toDosList: ArrayList<ToDo>,
    val itemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ToDosAdapter.ToDoViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate (R.layout.item_todo, parent, false)
        return ToDoViewHolder(view)
    }

    override fun onBindViewHolder(holder: ToDoViewHolder, position: Int) {
        holder.bind(toDosList[position], itemClickListener)
    }


    inner class ToDoViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(
            task: ToDo,
            clickListener: OnItemClickListener
        ){
            view.todoTxt.text = task.name
            view.setOnClickListener{ clickListener.onItemClicked(task)}
        }

    }

    override fun getItemCount(): Int {
        return toDosList.count()
    }


}

interface OnItemClickListener{
    fun onItemClicked(task: ToDo)
}