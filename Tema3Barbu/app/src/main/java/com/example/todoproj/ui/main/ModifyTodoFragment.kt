package com.example.todoproj.ui.main

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.todoproj.MainActivity
import com.example.todoproj.R
import com.example.todoproj.model.ToDo
import kotlinx.android.synthetic.main.fragment_modify_todo.*
import java.text.SimpleDateFormat
import java.util.*


class ModifyTodoFragment : Fragment() {

    companion object {
        const val ARG_NAME = "arg_name"

        var task: ToDo?= null
        private lateinit var alarmManager: AlarmManager

        fun newInstance(name: String): ModifyTodoFragment{
            val bundle = Bundle()
            bundle.putString(ARG_NAME, name)

            val fragment = ModifyTodoFragment()
            fragment.arguments = bundle
            return  fragment
        }

        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View =  inflater.inflate(R.layout.fragment_modify_todo, container, false)
        return view
    }

    private fun createChannel(){
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.O){
            val channel=NotificationChannel("notify","Channel",NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager= context?.getSystemService(NotificationManager::class.java)
                    as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    @SuppressLint("SimpleDateFormat")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        task = (activity as MainActivity).task
        val calendar = Calendar.getInstance()
        createChannel()
        tv_time.text= SimpleDateFormat("HH:mm:ss").format(task!!.date.time)
        tv_date.text=SimpleDateFormat("dd/MM/yyyy").format(task!!.date.time)

        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        btn_time.setOnClickListener {

            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                calendar.set(Calendar.HOUR_OF_DAY,hour)
                calendar.set(Calendar.MINUTE,minute)
                calendar.set(Calendar.SECOND,0)
                tv_time.text=SimpleDateFormat("HH:mm:ss").format(calendar.time)
            }
            TimePickerDialog(context,timeSetListener,calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE),true).show()
        }

        btn_date.setOnClickListener {
            val dpd = DatePickerDialog(context!!, DatePickerDialog.OnDateSetListener { view, year1, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR,year1)
                calendar.set(Calendar.MONTH,monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth)

                tv_date.text = SimpleDateFormat("dd/MM/yyyy").format(calendar.time)

                calendar.get(Calendar.YEAR)
                calendar.get(Calendar.MONTH)
                calendar.get(Calendar.DAY_OF_MONTH)
            }, year, month, day)
            dpd.show()
        }

        btn_create.setOnClickListener {
            Toast.makeText(context,"The update was successful!",Toast.LENGTH_SHORT).show()

            alarmManager = context?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, MyAlarmReceiver::class.java)
            alarmManager.set(
                AlarmManager.RTC_WAKEUP,
                calendar.timeInMillis,
                PendingIntent.getBroadcast(context,0,intent,0)
            )
            task!!.date = calendar.time
        }

    }




}