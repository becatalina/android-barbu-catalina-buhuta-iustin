package com.example.todoproj.ui.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.todoproj.R

class MyAlarmReceiver: BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val builder = NotificationCompat.Builder(context, "notify")
            .setSmallIcon(R.drawable.button_selected)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentTitle("To Do")
            .setContentText("Alarm was set")

        val notificationManager=NotificationManagerCompat.from(context)

        notificationManager.notify(50,builder.build())
    }
}