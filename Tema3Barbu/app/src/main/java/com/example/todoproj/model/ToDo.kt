package com.example.todoproj.model

import java.sql.Time
import java.util.*

data class ToDo( var id: Int, var name: String, var date: Date)