package com.example.todoproj.ui.main

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.todoproj.MainActivity
import com.example.todoproj.R
import com.example.todoproj.adapters.OnItemClickListener
import com.example.todoproj.adapters.ToDosAdapter
import com.example.todoproj.model.ToDo
import kotlinx.android.synthetic.main.main_fragment.*
import java.util.*
import kotlin.collections.ArrayList

class MainFragment : Fragment(), OnItemClickListener {

    companion object {
        const val ARG_NAME = "arg_name"

        fun newInstance(name: String): MainFragment{
            val bundle = Bundle()
            bundle.putString(ARG_NAME, name)

            val fragment = MainFragment()
            fragment.arguments = bundle
            return  fragment
        }
    }

    var todoList: ArrayList<ToDo>? = null
    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        displayToDos()

    }

    fun displayToDos(){
        todoList = ArrayList()
        val cal = Calendar.getInstance()
        todoList!!.add( ToDo(1, "Walk the first dog", date = cal.time))
        todoList!!.add(ToDo(2,"Walk the cat", date = cal.time))

        val layoutManager = LinearLayoutManager(context)
        val toDosAdapter = todoList?.let { ToDosAdapter(it, this) }
        tv_todo.layoutManager = layoutManager
        tv_todo.adapter = toDosAdapter

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

    }

    override fun onItemClicked(task: ToDo) {
        Log.i("task", task.name)
        (activity as MainActivity).task = task
        (activity as MainActivity).changeFragment()
    }

}