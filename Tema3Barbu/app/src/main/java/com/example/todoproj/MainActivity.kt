package com.example.todoproj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.todoproj.model.ToDo
import com.example.todoproj.ui.main.MainFragment
import com.example.todoproj.ui.main.ModifyTodoFragment

class MainActivity : AppCompatActivity() {

    companion object{
        enum class FragmentTags(val value: String) {
            TAG_MAIN_FRAGMENT("TAG_MAIN_FRAGMENT"),
            TAG_MODIFY_FRAGMENT("TAG_MODIFY_FRAGMENT")
        }

    }

    var task: ToDo? = null
    var currentFragmentString: FragmentTags? = FragmentTags.TAG_MAIN_FRAGMENT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance("Awake"))
                    .commitNow()
        }
    }

    fun changeFragment(){
        val currentFragmentString = when(this.currentFragmentString){
            null -> FragmentTags.TAG_MAIN_FRAGMENT
            FragmentTags.TAG_MAIN_FRAGMENT -> FragmentTags.TAG_MODIFY_FRAGMENT
            FragmentTags.TAG_MODIFY_FRAGMENT -> FragmentTags.TAG_MAIN_FRAGMENT
        }
        replaceFragment(currentFragmentString)
    }

    private fun replaceFragment(TAG: FragmentTags){
        TAG.run {
            val fragment: Fragment = when(this){
                FragmentTags.TAG_MAIN_FRAGMENT -> MainFragment.newInstance("Awake")
                FragmentTags.TAG_MODIFY_FRAGMENT -> ModifyTodoFragment.newInstance("Awake")
            }
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, fragment, this.value).commitNow()

        }
    }
}