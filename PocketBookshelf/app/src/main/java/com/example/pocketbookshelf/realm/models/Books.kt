package com.example.pocketbookshelf.realm.models


import io.realm.RealmObject
import io.realm.RealmList
import io.realm.annotations.PrimaryKey
import java.io.Writer

open class Books() : RealmObject() {

    @PrimaryKey
    var BookId: Long = 0
        get() = field

    var Title: String = ""
        get() = field

    var Description: String = ""
        get() = field

    var PageCount: Int = 0
        get() = field

    var ImagePath: String? = null
        get() = field
        set(value) { field = value }

    var Genre: Genres? = null
        get() = field

    var ListOfReviews: RealmList<Reviews> = RealmList()
        get() = field
        set(value) { field = value}

    var ListOfAuthors: RealmList<Writers> = RealmList()
        get() = field

    constructor(i: Long, t: String, d: String, p: Int, ip:String?, g: Genres?, a: Writers?):this(){
        BookId = i
        Title = t
        Description = d
        PageCount = p
        ImagePath = ip
        Genre = g
        ListOfAuthors.add(a)
    }

}