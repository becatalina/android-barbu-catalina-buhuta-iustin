package com.example.pocketbookshelf.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.realm.models.Reviews

class ReviewsAdapter (private val mReviews: List<Reviews>, private val mUsersName: List<String?>):
    RecyclerView.Adapter<ReviewsAdapter.ViewHolder>(){
    inner class ViewHolder(listItemView: View): RecyclerView.ViewHolder(listItemView){
        val username = itemView.findViewById<TextView>(R.id.tv_username_review)
        val rating = itemView.findViewById<TextView>(R.id.tv_rating_review)
        val comment = itemView.findViewById<TextView>(R.id.tv_comment_review)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val reviewView = inflater.inflate(R.layout.review_layout,parent,false)
        return ViewHolder(reviewView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val review : Reviews = mReviews[position]
        holder.username.text = mUsersName[position]
        holder.comment.text = review.Content
        holder.rating.text= review.Rating.toString()+"/10"
    }

    override fun getItemCount(): Int {
        return mReviews.size
    }
}