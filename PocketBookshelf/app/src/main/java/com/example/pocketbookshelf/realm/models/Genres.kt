package com.example.pocketbookshelf.realm.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Genres() : RealmObject() {

    @PrimaryKey
    var GenreId: Long = 0
        get() = field

    var Type: String = ""
        get() = field

    constructor(i: Long, t: String):this(){
        GenreId = i
        Type = t
    }

}