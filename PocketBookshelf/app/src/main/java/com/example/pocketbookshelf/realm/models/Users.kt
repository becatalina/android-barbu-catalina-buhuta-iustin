package com.example.pocketbookshelf.realm.models
import io.realm.RealmObject
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Users() : RealmObject() {

    @PrimaryKey
    var UserId: Long = 0
        get() = field

    var Username: String = ""
        get() = field

    var Email: String = ""
        get() = field

    var Password: String = ""
        get() = field

    var ListOfShelves: RealmList<Shelves >? = RealmList<Shelves>()
        get() = field

    constructor(i: Long, u:String, e:String, p: String):this(){
        UserId = i
        Username = u
        Password = p
        Email = e

    }

}