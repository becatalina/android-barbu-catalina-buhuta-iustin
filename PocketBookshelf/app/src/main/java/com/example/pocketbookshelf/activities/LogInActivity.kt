package com.example.pocketbookshelf.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pocketbookshelf.fragments.LogInFragment
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.fragments.RegisterFragment

class LogInActivity:AppCompatActivity() {
    companion object{
        enum class FragmentsTag(val value:String){
            TAG_LOGIN("TAG_LOGIN"),
            TAG_REGISTER("TAG_REGISTER")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity_layout)

        replaceFragment(FragmentsTag.TAG_LOGIN)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(supportFragmentManager.backStackEntryCount<1)
            finish()
    }
    fun replaceFragment(TAG: FragmentsTag?){
        TAG?.run {
            val fragment = when(this){
                FragmentsTag.TAG_LOGIN -> LogInFragment.newInstance()
                FragmentsTag.TAG_REGISTER -> RegisterFragment.newInstance()
            }
            val transaction=supportFragmentManager.beginTransaction()
            transaction.add(R.id.fl_activity_layout,fragment,value).addToBackStack(TAG.value).commit()
        }
    }
}