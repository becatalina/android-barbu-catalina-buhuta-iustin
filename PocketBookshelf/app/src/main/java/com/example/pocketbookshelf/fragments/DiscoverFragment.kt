package com.example.pocketbookshelf.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketbookshelf.adapters.BooksAdapter
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.activities.CameraActivity
import com.example.pocketbookshelf.realm.RealmHelper
import com.example.pocketbookshelf.realm.models.Books
import kotlinx.android.synthetic.main.discover_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

class DiscoverFragment : Fragment(){
    companion object{
        const val ARG_NAME = "user_id"
        fun newInstance(userId : Long): DiscoverFragment {
            val fragment =
                DiscoverFragment()

            val bundle = Bundle().apply {
                putLong(ARG_NAME, userId)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    lateinit var realmHelper: RealmHelper
    private var userId: Long = -1
    private val LAUNCH_CAMERA_ACTIVITY = 1
    private var cameraResult: String? = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.discover_layout,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userId = arguments?.getLong(ToReadFragment.ARG_NAME)!!
        connectToRealm()

        displayAllBooks()

        btn_camera.setOnClickListener{
            val intent = Intent(context,
                CameraActivity::class.java)
            startActivityForResult(intent, LAUNCH_CAMERA_ACTIVITY)


        }

        btn_search.setOnClickListener {
            if(et_search.text.toString().length > 3){
                val result = realmHelper.searchBookByName(et_search.text.toString())
                if (result.size > 0){
                    val adapter =
                        BooksAdapter(
                            result,
                            userId
                        )
                    rv_all_books.adapter = adapter
                    rv_all_books.layoutManager = LinearLayoutManager(context)
                }
                else{
                    Toast.makeText(context,"No books found", Toast.LENGTH_LONG).show()
                }
            }
        }

        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(et_search.text.isEmpty()){
                    displayAllBooks()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == LAUNCH_CAMERA_ACTIVITY){
            if(resultCode == Activity.RESULT_OK){
                cameraResult = data?.getStringExtra("result")?.replace("\n", " ")
                et_search.setText(cameraResult.toString().toLowerCase(Locale.ROOT))

            }
        }
        if(resultCode == Activity.RESULT_CANCELED){
            Toast.makeText(context, "No text detected", Toast.LENGTH_LONG).show()
        }
    }

    private fun displayAllBooks(){
        doAsync {
            var books : ArrayList<Books> = ArrayList()
            try {
                books = realmHelper.getAllBooks()
            } catch (e: Exception) {
                Log.e("ERROR", e.message.toString())
            }
            uiThread {
                var adapter =
                    BooksAdapter(
                        books,
                        userId
                    )
                rv_all_books.adapter = adapter
                rv_all_books.layoutManager = LinearLayoutManager(context)
            }
        }
    }

    fun connectToRealm() {
        try{
            realmHelper = RealmHelper()
            realmHelper.connectToRealm(context!!)

        }catch (e: Exception){
            Log.i("INFO", e.toString())
        }

    }
}