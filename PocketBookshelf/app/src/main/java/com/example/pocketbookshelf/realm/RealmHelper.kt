package com.example.pocketbookshelf.realm

import android.content.Context
import android.graphics.Bitmap
import com.example.pocketbookshelf.realm.models.*
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmList
import io.realm.RealmResults
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList

open class Configuration{
    val configFile = "bookshelf2.realm"
    val config: RealmConfiguration = RealmConfiguration.Builder().name(configFile).build()
}


class RealmHelper() {


    lateinit var config: RealmConfiguration


    fun addBooks(){
        val realm = Realm.getInstance(config)

        //Writers
        val alain = Writers(0,"Alain","Briottet","",null,null)
        val conan = Writers(1,"Conan","Grace","",null,null)
        val daniel = Writers(2,"Daniel","O'Leary","",null,null)
        val lili = Writers(3,"Lili","Wright","",null,null)
        val lina = Writers(4,"Lina","Cabeza-Vanegas","",null,null)
        val andrzej = Writers(5,"Andrzej","Sapkowski","",null,null)
        val leigh =Writers(6,"Leigh","Bardugo","",null,null)
        val teri = Writers(7,"Teri","Terry","",null,null)
        val mesha = Writers(8,"Mesha","Maren","",null,null)
        val chloe = Writers(9,"Chloe","Hooper","",null,null)
        val kathleen = Writers(10,"Kathleen", "Groger","",null,null)
        val christina = Writers(11,"Christina", "Henry","",null,null)
        val jrr = Writers(12,"J. R. R. ","Tolkien","",null,null)
        val stephen = Writers(13,"Stephen", "King", "",null,null)
        val rebecca = Writers (14, "Rebecca", "Ross","",null,null)
        val charlotte = Writers(15,"Charlotte E.","English","",null,null)

        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(alain) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(conan) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(daniel) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(lili) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(lina) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(andrzej) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(leigh) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(teri) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(mesha) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(chloe) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(kathleen) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(christina) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(jrr) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(stephen) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(rebecca) }
        realm.executeTransaction{realm -> val w: Writers = realm.copyToRealmOrUpdate(charlotte) }

        //Genres
        val romance = Genres(0,"Romance")
        val horror = Genres(1, "Horror")
        val adventure =Genres(2, "Adventure")
        val fantasy =Genres(3, "Fantasy")
        val sf =Genres(4, "SF")
        val mystery =Genres(5, "Mystery")
        val thriller =Genres(6, "Thriller")
        val classics =Genres(7, "Classics")

        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(romance) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(horror) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(adventure) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(fantasy) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(sf) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(mystery) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(thriller) }
        realm.executeTransaction { realm -> val p: Genres = realm.copyToRealm(classics) }

        //Books
        val boston = Books(0,"Boston","No description",300,"/drawable/boston",romance,alain)
        val childhood = Books(1,"Childhood","No description",234,"/drawable/childhood",horror,conan)
        val dtmd = Books(2,"Dancing to my death","In the summer of 2018 Daniel O Leary received the news that we " +
                "all dread a cancer diagnosis. As a priest, teacher, bestselling author and retreat facilitator, it " +
                "was a natural instinct for Daniel to journal his thoughts and feelings during his cancer journey. " +
                "Completed just before his death in January 2019, this book is an incredibly raw and courageous account. " +
                "It pulls no punches in terms of Daniel s struggles to cope with his diagnosis, the challenges of cancer " +
                "treatment and the emotional rollercoaster of facing his own death. The book reveals a soul in chaos. It " +
                "has the extremes of a torn kite in a storm it sweeps and swoops between hope and despair, throws cartwheels " +
                "and steadies out, crashes with fear and continues with raw and real courage. During his final illness Daniel " +
                "found a great clarity about what is important in life. There is a tough honesty here: an honesty that can only " +
                "emerge when the circus of religious activities leaves town, and when people are encouraged to really explore what " +
                "their Christianity means to them",328,"/drawable/dancing_to_my_death",romance,daniel)
        val dwtt = Books(3, "Dancing with the tiger","When 30-year-old Anna Ramsey learns that a meth-addicted looter has dug up what " +
                "might be the funerary mask of Montezuma, she books the next flight to Oaxaca. Determined to redeem her father, a discredited " +
                "art collector, and to one-up her unfaithful fiancé, a museum curator, Anna hurls herself headlong into Mexico’s underground " +
                "art world.",175,"/drawable/dancing_with_the_tiger",thriller,lili)
        val dcb = Books(4,"Don't come back","In this collection of linked lyrical and narrative essays, experimental translations, and reinterpreted" +
                " myths, Lina Maria Ferreira Cabeza-Vanegas launches into an exploration of home and identity, family history and belonging, continually" +
                " examining what it means to feel familiarity but never really feel at home.",198,"/drawable/don_t_come_back",classics,lina)
        val sos = Books(5,"Season of storms","Geralt of Rivia is a Witcher, one of the few capable of hunting the monsters that prey on humanity. A mutant" +
                " who is tasked with killing unnatural beings. He uses magical signs, potions, and the pride of every Witcher - two swords, steel and" +
                " silver.", 236,"/drawable/season_of_storms",fantasy,andrzej)
        val soc = Books(6,"Six of crows","No description", 267,"/drawable/six_of_crows",fantasy,leigh)
        val slate = Books(7,"Slate","Kyla’s memory has been erased,\n" + "her personality wiped blank,\n" + "her memories lost for ever.\n"+ "She’s been" +
                " Slated.\n" + "The government claims she was a terrorist and that they are giving her a second chance - as long as she plays by their rules." +
                " But echoes of the past whisper in Kyla’s mind. Someone is lying to her, and nothing is as it seems. Who can she trust in her search for the" +
                " truth?",213,"/drawable/slate",thriller,teri)
        val sugar = Books(8,"Sugar run","No description", 223,"/drawable/sugar_run",adventure,mesha)
        val arsonist = Books(9,"The arsonist","No description",165,"/drawable/the_arsonist",classics,chloe)
        val tboy = Books(10, "The book of you","No description",134,"/drawable/the_book_of_you",classics,alain)
        val colony = Books(11,"The colony", "No description",367,"/drawable/the_colony",fantasy,kathleen)
        val tgir = Books(12,"The girl in red","No description", 412,"/drawable/the_girl_in_red",classics,christina)
        val hobbit = Books(13,"The hobbit","No description",598,"/drawable/the_hobbit",fantasy,jrr)
        val outsider=Books(14,"The outsider","No description", 371,"/drawable/the_outsider",fantasy,stephen)
        val tqr=Books(15,"The queen's rising","No description",211,"/drawable/the_queen_rising",adventure,rebecca)
        val tzc= Books(16,"The Zolin conspiracy","No description",133,"/drawable/the_zolin_conspiracy",sf,charlotte)

        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(boston) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(childhood) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(dtmd) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(dwtt) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(dcb) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(sos) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(soc) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(slate) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(sugar) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(arsonist) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(tboy) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(colony) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(tgir) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(hobbit) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(outsider) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(tqr) }
        realm.executeTransaction{realm -> val b: Books = realm.copyToRealmOrUpdate(tzc) }

        realm.close()
    }



    fun connectToRealm(context: Context){
        Realm.init(context)
        config = Configuration().config
        //addBooks()


    }

    fun addUserToRealmAsync(name: String, e:String, p:String ) {
        val realmInstance = Realm.getInstance(config)

        var lastId: Long
        try {
            lastId =realmInstance.where<Users>(Users::class.java).findAll().last()?.UserId ?: 0
        }
        catch (e:Exception){
            lastId = 0
        }
        lastId += 1
        val user = Users(lastId, name, e, p)

        var lastShelfId: Long
        try {
            lastShelfId = realmInstance.where<Shelves>(Shelves::class.java).findAll().last()?.ShelfId ?: 0
        }catch (e:Exception){
            lastShelfId = 0
        }

        var list: RealmList<Shelves> = RealmList<Shelves>()
        list.add(Shelves(lastShelfId.plus(1), "To Read", RealmList<Books>(), user))
        list.add(Shelves(lastShelfId.plus(2), "Reading", RealmList<Books>(), user))
        list.add(Shelves(lastShelfId.plus(3), "Read", RealmList<Books>(), user)  )
        user.ListOfShelves = list

            realmInstance.executeTransaction{
                realmInstance->val u: Users = realmInstance.copyToRealmOrUpdate(user)
            }

        }


    fun addReviewToRealmAsync(review: Reviews,book: Books){
        val realmInstance = Realm.getInstance(config)
        var lastId: Long
        try {
            lastId = realmInstance.where<Reviews>(Reviews::class.java).findAll().last()?.ReviewId ?: 0
        }
        catch (e:Exception){
            lastId = 0
        }
        lastId+=1
        review.ReviewId = lastId
        realmInstance.executeTransaction{
            realmInstance->val r: Reviews = realmInstance.copyToRealmOrUpdate(
            review
        )
        }
        realmInstance.beginTransaction()
        book.ListOfReviews.add(review)
        realmInstance.commitTransaction()

    }

    fun addReviewToRealmAsync(user: Users?, content: String, rating : Int ){
        val realmInstance = Realm.getInstance(config)
        var lastId: Long =
            realmInstance.where<Reviews>(Reviews::class.java).findAll().last()?.ReviewId ?: 0
        lastId += 1
        realmInstance.executeTransaction{
                realmInstance->val r: Reviews = realmInstance.copyToRealmOrUpdate(
            Reviews(
                lastId, content, rating, user
            )
        )
        }

    }





    fun usernameExists(username: String):Users? {

        var user:Users?  = null
        val realm: Realm = Realm.getInstance(config)
        val users = realm.where<Users>(Users::class.java).findAll()

            val result: Users? = realm.where<Users>(Users::class.java).equalTo("Username",username).findFirst()
            if( result != null){
                user = Users(result.UserId,result.Username, result.Email, result.Password)

            }

        return user


    }

    fun emailExists(email: String): Users? {

        var user:Users?  = null
        val realm: Realm = Realm.getInstance(config)

        val result: Users? = realm.where<Users>(Users::class.java).equalTo("Email",email).findFirst()
        if( result != null){
            user = Users(result.UserId,result.Username, result.Email, result.Password)

        }
        realm.close()


        return user

    }

    fun getUserById(id: Long): Users?{
        var user:Users?  = null
        val realm: Realm = Realm.getInstance(config)

        val result: Users? = realm.where<Users>(Users::class.java).equalTo("UserId",id).findFirst()
        if( result != null){
            user = Users(result.UserId,result.Username, result.Email, result.Password)
        }
        realm.close()


        return user

    }

    fun getShelves(userId:Long): ArrayList<Shelves>{
        var shelves = ArrayList<Shelves>()
        val realm : Realm =Realm.getInstance(config)

        var resultUser: RealmResults<Shelves>? = realm.where<Shelves>(Shelves::class.java).findAll()
        if (resultUser != null) {

            for(r in resultUser){
                if(r.User?.UserId == userId) {

                    var books = RealmList<Books>()
                    for (b in r.ListOfBooks!!){
                        books.add(Books(b.BookId, b.Title, b.Description, b.PageCount, b.ImagePath
                        ,b.Genre, b.ListOfAuthors[0]))
                    }

                    shelves.add(
                        Shelves(r.ShelfId, r.Title, books, null)
                    )
                }
            }
        }
        realm.close()
         return shelves

    }


    fun getBook(bookId:Long): Books?{
        val realm : Realm =Realm.getInstance(config)
        var resultsBook: RealmResults<Books>? = realm.where<Books>(Books::class.java).findAll()
        if(resultsBook !=null)
        for(b in resultsBook){
            if(b.BookId == bookId)
                return b
        }

        return null
    }



    fun getUserReviews(userId: Long): ArrayList<Reviews>{
        var reviews = ArrayList<Reviews>()
        val realm: Realm = Realm.getInstance(config)

        var result :RealmResults<Reviews>? = realm.where<Reviews>(Reviews::class.java).findAll()
        if(result != null){
            for (r in result){
                if(r.AuthorId!=null)
                    if(r.AuthorId!!.UserId == userId){
                        reviews.add(
                            Reviews(r.ReviewId, r.Content, r.Rating, r.AuthorId)
                        )
                    }
            }
        }
        //realm.close()
        return reviews
    }

    fun getAllBooks(): ArrayList<Books>{
        val realm : Realm =Realm.getInstance(config)
        var books = ArrayList<Books>()
        var resultsBook: RealmResults<Books>? = realm.where<Books>(Books::class.java).findAll()
        if (resultsBook != null) {
            for(b in resultsBook)
                books.add(
                    Books(b.BookId, b.Title,b.Description,b.PageCount,b.ImagePath,b.Genre,b.ListOfAuthors[0])
                )
        }
        realm.close()
        return books
    }

    fun addBookToShelf(userId: Long,book: Books,shelf: Int){
        var shelves = ArrayList<Shelves>()
        val realm : Realm =Realm.getInstance(config)

        var resultShelves: RealmResults<Shelves>? = realm.where<Shelves>(Shelves::class.java).findAll()
        if (resultShelves != null) {

            for(r in resultShelves){
                if(r.User?.UserId == userId) {
                    shelves.add(r)
                }
            }
        }
        realm.beginTransaction()
        shelves[shelf].ListOfBooks?.add(book)
        realm.commitTransaction()
        realm.close()
    }

    fun removeBookFromShelf(userId: Long,book: Books,shelf: Int){
        var shelves = ArrayList<Shelves>()
        val realm : Realm =Realm.getInstance(config)

        var resultShelves: RealmResults<Shelves>? = realm.where<Shelves>(Shelves::class.java).findAll()
        if (resultShelves != null) {

            for(r in resultShelves){
                if(r.User?.UserId == userId) {
                    shelves.add(r)
                }
            }
        }
        var bookDelete: Books = Books()
        for(b in shelves[shelf].ListOfBooks!!)
            if(b.BookId==book.BookId){
                bookDelete=b
                break
            }
        realm.beginTransaction()
        shelves[shelf].ListOfBooks?.remove(bookDelete)
        realm.commitTransaction()
        realm.close()
    }

    fun searchBookByName(book: String): ArrayList<Books>{
        var books = ArrayList<Books>()
        val realm:Realm =  Realm.getInstance(config)
        val strings = book.split(" ")

        var resultBooks: RealmResults<Books>? = realm.where<Books>(Books::class.java).findAll()
        if(resultBooks != null){

            for (b in resultBooks) {
                for(s in strings){
                    if(b.Title.toLowerCase(Locale.ROOT).contains(s.toLowerCase(Locale.ROOT)) or
                        (b.ListOfAuthors[0] as Writers).FirstName.toLowerCase(Locale.ROOT).contains(s.toLowerCase(
                            Locale.ROOT)) or
                        (b.ListOfAuthors[0] as Writers).LastName.toLowerCase(Locale.ROOT).contains(s.toLowerCase(Locale.ROOT))
                    ) {
                        val foundBook =  Books(
                            b.BookId, b.Title, b.Description, b.PageCount, b.ImagePath, b.Genre,
                            b.ListOfAuthors[0]
                        )
                        foundBook.ListOfReviews = b.ListOfReviews
                        books.add(foundBook)
                        break

                    }
                }

            }

        }

        return books

    }

    fun checkForReview(book: Books, userId: Long): Boolean{
        val reviews =book.ListOfReviews
        if (reviews != null) {
            for(r in reviews)
                if(r.AuthorId?.UserId == userId)
                    return false
        }
        return true
    }



    }













