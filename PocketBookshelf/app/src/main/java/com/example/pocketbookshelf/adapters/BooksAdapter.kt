package com.example.pocketbookshelf.adapters

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pocketbookshelf.BuildConfig
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.activities.BookViewActivity
import com.example.pocketbookshelf.realm.models.Books
import kotlinx.android.synthetic.main.book_in_shelf_layout.view.*


class BooksAdapter (private val mBooks: List<Books>, private val userId: Long) :
    RecyclerView.Adapter<BooksAdapter.ViewHolder>(){
    inner class ViewHolder(listItemView: View): RecyclerView.ViewHolder(listItemView){
        val bookImg = itemView.findViewById<ImageButton>(R.id.imgBtn_book_shelf)
        val bookTitleTV = itemView.findViewById<TextView>(R.id.tv_book_title)
        val context = listItemView.context

        fun bind(book: Books){
            itemView.tv_book_title.text = book.Title
            if(book.ImagePath != "")
                itemView.imgBtn_book_shelf.setImageURI(Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + book.ImagePath))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val bookView = inflater.inflate(R.layout.book_in_shelf_layout,parent,false)
        return ViewHolder(bookView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val book : Books= mBooks[position]

        viewHolder.bind(book)
        viewHolder.bookImg.setOnClickListener {
            val intent = Intent(viewHolder.context,
                BookViewActivity()::class.java).putExtra("book",book.BookId)
                .putExtra("user_id", userId)
            viewHolder.context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mBooks.size
    }
}