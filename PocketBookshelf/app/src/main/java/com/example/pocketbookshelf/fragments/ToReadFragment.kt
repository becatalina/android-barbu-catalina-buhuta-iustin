package com.example.pocketbookshelf.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.adapters.ShelvesAdapter
import com.example.pocketbookshelf.realm.RealmHelper
import com.example.pocketbookshelf.realm.models.Shelves
import kotlinx.android.synthetic.main.to_read_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class ToReadFragment : Fragment() {
    companion object{
        const val ARG_NAME = "user_id"
        fun newInstance(userId : Long): ToReadFragment {
            val fragment = ToReadFragment()

            val bundle = Bundle().apply {
                putLong(ARG_NAME, userId)
            }
            fragment.arguments = bundle
            return fragment
        }
    }


    lateinit var realmHelper: RealmHelper
    var userId: Long = -1
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.to_read_layout,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userId = arguments?.getLong(ARG_NAME)!!
        refresh()
    }

    fun refresh(){
        connectToRealm()
        displayShelves()
    }

    fun displayShelves(){
        doAsync {
            var shelves : ArrayList<Shelves> = ArrayList()
            try {
                shelves = realmHelper.getShelves(userId)

            } catch (e: Exception) {
                Log.e("ERROR", e.message.toString())
            }

            uiThread {
                var adapter =
                    ShelvesAdapter(
                        shelves,
                        context!!,
                        userId
                    )
                rv_shelves_toRead.adapter = adapter
                rv_shelves_toRead.layoutManager = LinearLayoutManager(context)
            }
        }

    }
    private fun connectToRealm() {
        try{
            realmHelper = RealmHelper()
            realmHelper!!.connectToRealm(context!!)

        }catch (e: Exception){
            Log.i("INFO", e.toString())
        }

    }
}

