package com.example.pocketbookshelf.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.realm.models.Books
import com.example.pocketbookshelf.realm.models.Shelves
import kotlinx.android.synthetic.main.shelf_layout.view.*

class ShelvesAdapter (private val mShelves:List<Shelves>, private val context: Context, private val mUserId:Long) :
    RecyclerView.Adapter<ShelvesAdapter.ViewHolder>() {
    inner class ViewHolder(listItemView: View): RecyclerView.ViewHolder(listItemView){
        val shelfTitleTV = itemView.findViewById<TextView>(R.id.tv_shelf_title)
        val booksRV = itemView.findViewById<RecyclerView>(R.id.rv_books)

        fun bind(shelf: Shelves){
            itemView.tv_shelf_title.text = shelf.Title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val shelfView = inflater.inflate(R.layout.shelf_layout,parent,false)
        return ViewHolder(shelfView)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val shelf : Shelves = mShelves[position]
        //get books from shelves
        val booksFromShelf : ArrayList<Books> = ArrayList<Books>()
        val result = shelf.ListOfBooks!!
        for (r in result){
                booksFromShelf.add(
                    Books(r.BookId, r.Title, r.Description, r.PageCount, r.ImagePath, r.Genre, r.ListOfAuthors[0])
                )
            }

        viewHolder.bind(shelf)


        var adapter = BooksAdapter(
            booksFromShelf,
            mUserId
        )
        val booksRV = viewHolder.booksRV
        booksRV.adapter = adapter
        booksRV.layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
    }

    override fun getItemCount(): Int {
        return mShelves.size
    }
}