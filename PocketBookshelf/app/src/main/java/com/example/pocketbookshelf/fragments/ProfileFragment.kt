package com.example.pocketbookshelf.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.adapters.ReviewsAdapter
import com.example.pocketbookshelf.activities.LogInActivity
import com.example.pocketbookshelf.realm.RealmHelper
import com.example.pocketbookshelf.realm.models.Reviews
import kotlinx.android.synthetic.main.book_info_layout.rv_reviews
import kotlinx.android.synthetic.main.profile_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ProfileFragment : Fragment() {
    companion object{
        const val ARG_NAME = "user_id"
        fun newInstance(userId : Long): ProfileFragment {
            val fragment =
                ProfileFragment()

            val bundle = Bundle().apply {
                putLong(ARG_NAME, userId)
            }
            fragment.arguments = bundle
            return fragment
        }
    }
    var realmHelper = RealmHelper()
    var userId: Long = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.profile_layout,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        realmHelper.connectToRealm(context!!)
        userId = arguments?.getLong(ARG_NAME) ?: -1

        displayReviews()
        setup()

        btn_logout.setOnClickListener {
            val intent = Intent(context,
                LogInActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
    private fun displayReviews(){
        doAsync {
            var reviews : ArrayList<Reviews> = ArrayList()
            var usersName: ArrayList<String?> = ArrayList()
            reviews = realmHelper.getUserReviews(userId)
            for(u in reviews)
                usersName.add(u.AuthorId?.Username)
            uiThread {
                var adapter =
                    ReviewsAdapter(
                        reviews,
                        usersName
                    )
                rv_reviews.adapter = adapter
                rv_reviews.layoutManager = LinearLayoutManager(context)
            }
        }

    }
    private fun setup(){
        //get user
        val user = realmHelper.getUserById(userId)

        tv_username_profile.text = user?.Username
        tv_email_profile.text = user?.Email

    }
}