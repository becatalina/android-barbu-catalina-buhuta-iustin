package com.example.pocketbookshelf.realm.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Reviews()    : RealmObject() {

    @PrimaryKey
    var ReviewId: Long = 0
        get() = field

    var Content: String = ""
        get() = field

    var Rating: Int = 0
        get() = field

    var AuthorId: Users? = null
        get() = field

    constructor(i:Long, c: String, r: Int, a: Users?):this(){
        ReviewId = i
        Content=c
        Rating = r
        AuthorId = a
    }

}