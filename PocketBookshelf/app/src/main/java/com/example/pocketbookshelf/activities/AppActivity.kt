package com.example.pocketbookshelf.activities

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.pocketbookshelf.fragments.DiscoverFragment
import com.example.pocketbookshelf.fragments.ProfileFragment
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.fragments.ToReadFragment
import kotlinx.android.synthetic.main.app_activity_layout.*

class AppActivity : AppCompatActivity() {
    companion object{
        enum class FragmentTag(val value: String){
            TAG_TO_READ("TAG_TO_READ"),
            TAG_DISCOVER("TAG_DISCOVER"),
            TAG_PROFILE("TAG_PROFILE")
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.app_activity_layout)


        replaceFragment(FragmentTag.TAG_TO_READ)

        imgBtn_toRead.setOnClickListener {
            replaceFragment(FragmentTag.TAG_TO_READ)
        }
        imgBtn_discover.setOnClickListener {
            replaceFragment(FragmentTag.TAG_DISCOVER)
        }
        imgBtn_profile.setOnClickListener {
            replaceFragment(FragmentTag.TAG_PROFILE)
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(supportFragmentManager.backStackEntryCount<1)
            finish()
    }

    override fun onResume() {
        super.onResume()
        val fragments: List<Fragment> =
            supportFragmentManager.fragments
        for (f in fragments) {
            if (f is ToReadFragment) f.refresh()
        }
    }



    private fun replaceFragment(TAG: FragmentTag?){
        TAG?.run {
            val fragment = when(this){
                FragmentTag.TAG_TO_READ -> bundleData()?.let {
                    ToReadFragment.newInstance(
                        it
                    )
                }
                FragmentTag.TAG_DISCOVER -> bundleData()?.let {
                    DiscoverFragment.newInstance(
                        it
                    )
                }
                FragmentTag.TAG_PROFILE -> bundleData()?.let {
                    ProfileFragment.newInstance(
                        it
                    )
                }
            }
            val transaction = supportFragmentManager.beginTransaction()
            if (fragment != null) {
                transaction.add(R.id.fl_app_layout,fragment,value).addToBackStack(TAG.value).commit()
            }
            else{
                Log.i("FRAGMENTS","Fragment could not be replaced")
            }
        }
    }

    private fun bundleData(): Long? {
        val bundle = intent.extras
        val value = bundle?.getLong("user_id")

        return value
    }



}