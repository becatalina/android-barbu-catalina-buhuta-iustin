package com.example.pocketbookshelf.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.activities.LogInActivity
import com.example.pocketbookshelf.realm.RealmHelper
import kotlinx.android.synthetic.main.register_layout.*
import java.lang.Exception


class RegisterFragment:Fragment() {
    companion object{
        fun newInstance()=
            RegisterFragment()
    }

    var realmHelper: RealmHelper? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.register_layout,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        connectToRealm()

        btn_register.setOnClickListener {
            if(!fieldValidation())
                return@setOnClickListener

            //add to database
            realmHelper?.addUserToRealmAsync(et_username.text.toString(), et_email.text.toString(),
                et_password.text.toString())

            Toast.makeText(context,getString(R.string.register_complete),Toast.LENGTH_SHORT).show()
            (activity as LogInActivity).onBackPressed()
        }
    }

    fun fieldValidation(): Boolean{
        if(et_username.text.toString() == "" || et_email.text.toString() == "" ||
            et_password.text.toString() == "" || et_conf_password.text.toString() == "") {
            Toast.makeText(context,getString(R.string.field_incomplete), Toast.LENGTH_LONG).show()
            return false
        }
        if(et_username.text.toString().length < 6) {
            Toast.makeText(context,getString(R.string.username_short),Toast.LENGTH_LONG).show()
            return false
        }
        if(et_username.text.toString()[0]==' ' ||
            et_username.text.toString()[et_username.text.toString().length-1] == ' '){
            Toast.makeText(context,getString(R.string.username_space),Toast.LENGTH_LONG).show()
            return false
        }
        if(!et_email.text.toString().contains('@') ||
            et_email.text.toString().contains(' ')) {
            Toast.makeText(context,getString(R.string.invalid_email), Toast.LENGTH_LONG).show()
            return false
        }
        if(et_password.text.toString().length < 6) {
            Toast.makeText(context,getString(R.string.password_short),Toast.LENGTH_LONG).show()
            return false
        }
        if(et_password.text.toString().contains(' ')) {
            Toast.makeText(context,getString(R.string.password_space),Toast.LENGTH_LONG).show()
            return false
        }
        if(et_password.text.toString() != et_conf_password.text.toString()) {
            Toast.makeText(context,getString(R.string.passwords_not_match),Toast.LENGTH_LONG).show()
            return false
        }



        //Verification if username isn't used
        if(realmHelper?.usernameExists(et_username.text.toString ())!= null) {
            Toast.makeText(context, getString(R.string.username_exists), Toast.LENGTH_LONG).show()
            return false
        }
        //Verification if email isn't used
        if(realmHelper?.usernameExists(et_email.text.toString ())!= null) {
            Toast.makeText(context, getString(R.string.email_already_exists), Toast.LENGTH_LONG).show()
            return false
        }


        return true
    }

    fun connectToRealm() {
        try{
            realmHelper = RealmHelper()
            realmHelper!!.connectToRealm(context!!)
            //realmHelper.addToGenres()
        }catch (e:Exception){
            Log.i("INFO", e.toString())
        }

    }


}