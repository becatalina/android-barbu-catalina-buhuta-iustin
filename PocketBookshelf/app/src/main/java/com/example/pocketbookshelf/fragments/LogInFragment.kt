package com.example.pocketbookshelf.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.activities.AppActivity
import com.example.pocketbookshelf.activities.LogInActivity
import com.example.pocketbookshelf.realm.RealmHelper
import kotlinx.android.synthetic.main.login_layout.*
import kotlinx.android.synthetic.main.login_layout.et_password
import kotlinx.android.synthetic.main.login_layout.et_username
import java.lang.Exception

class LogInFragment:Fragment() {
    companion object{
        fun newInstance()= LogInFragment()
    }

    private var realmHelper: RealmHelper? = null
    private var userid: Long? = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.login_layout,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        connectToRealm()


        btn_login_register.setOnClickListener {
            (activity as LogInActivity).replaceFragment(
                LogInActivity.Companion.FragmentsTag.TAG_REGISTER)
        }

        btn_login.setOnClickListener {
            if(!fieldValidation())
                return@setOnClickListener
            val intent = Intent(context,
                AppActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("user_id", userid)


            startActivity(intent)
        }
    }

    private fun fieldValidation(): Boolean{
        if(et_username.text.toString() == "" ||
                et_password.text.toString() == "") {
            Toast.makeText(context,getString(R.string.field_incomplete),Toast.LENGTH_LONG).show()
            return false
        }

        //verification that the email or username exist
        val user = realmHelper?.usernameExists(et_username.text.toString())
        if(user == null){
            Toast.makeText(context,getString(R.string.username_nonexistent),Toast.LENGTH_LONG).show()
            return false
        }

        if(!et_password.text.toString().equals(user.Password)){
            Toast.makeText(context,getString(R.string.password_incorrect),Toast.LENGTH_LONG).show()
            return false
        }
        userid = user.UserId
        return true
    }

    private fun connectToRealm() {
        try{
            realmHelper = RealmHelper()
            realmHelper!!.connectToRealm(context!!)

        }catch (e: Exception){
            Log.i("INFO", e.toString())
        }

    }



}