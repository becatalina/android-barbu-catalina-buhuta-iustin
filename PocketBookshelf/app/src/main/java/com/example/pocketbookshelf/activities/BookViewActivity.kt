package com.example.pocketbookshelf.activities

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pocketbookshelf.BuildConfig
import com.example.pocketbookshelf.R
import com.example.pocketbookshelf.adapters.ReviewsAdapter
import com.example.pocketbookshelf.realm.RealmHelper
import com.example.pocketbookshelf.realm.models.Books
import com.example.pocketbookshelf.realm.models.Reviews
import kotlinx.android.synthetic.main.book_info_layout.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class BookViewActivity() : AppCompatActivity() {

    var userId :Long = -1
    var bookId:Long =-1
    lateinit var realmHelper: RealmHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.book_info_layout)

        bookId = intent.extras?.get("book") as Long
        userId = intent.extras?.get("user_id") as Long

        connectToRealm()
        var book :Books? = realmHelper.getBook(bookId)
        tv_book_info_title.text=book?.Title
        var authors : String =""
        for(a in book?.ListOfAuthors!!)
            authors=a.FirstName+" " + a.LastName+" "

        tv_book_info_authors.text=authors
        tv_book_info_genres.text=book.Genre?.Type
        tv_book_info_page_count.text= book.PageCount.toString()
        tv_book_info_description.text= book.Description

        if(book.ImagePath != "")
            imgBtn_book.setImageURI(Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + book.ImagePath))
        displayReviews()

        btn_add_review.setOnClickListener {
            if(!realmHelper.checkForReview(book,userId))
            {
                Toast.makeText(this,R.string.review_already_added,Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            var review = Reviews()
            review.Content=et_comment.text.toString()
            val rating = rb_book.rating*2
            review.Rating = rating.toInt()
            review.AuthorId = realmHelper.getUserById(userId)
            realmHelper.addReviewToRealmAsync(review,book)
            Toast.makeText(this,R.string.review_added,Toast.LENGTH_SHORT).show()
            displayReviews()
        }

        btn_add_to_read.setOnClickListener {
            val shelves = realmHelper.getShelves(userId)
            for(b in shelves[2].ListOfBooks!!)
                if(b.BookId == book.BookId){
                    realmHelper.removeBookFromShelf(userId,book,2)
                    Toast.makeText(this,
                        R.string.book_already_exist,Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            realmHelper.addBookToShelf(userId,book,2)
            Toast.makeText(this,
                R.string.book_added,Toast.LENGTH_SHORT).show()

        }

        btn_add_to_toRead.setOnClickListener {
            val shelves = realmHelper.getShelves(userId)
            for(b in shelves[0].ListOfBooks!!)
                if(b.BookId == book.BookId){
                    realmHelper.removeBookFromShelf(userId,book,0)
                    Toast.makeText(this,
                        R.string.book_already_exist,Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            realmHelper.addBookToShelf(userId,book,0)
            Toast.makeText(this,
                R.string.book_added,Toast.LENGTH_SHORT).show()
        }

        btn_add_to_reading.setOnClickListener {
            val shelves = realmHelper.getShelves(userId)
            for(b in shelves[1].ListOfBooks!!)
                if(b.BookId == book.BookId){
                    realmHelper.removeBookFromShelf(userId,book,1)
                    Toast.makeText(this,
                        R.string.book_already_exist,Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            realmHelper.addBookToShelf(userId,book,1)
            Toast.makeText(this,
                R.string.book_added,Toast.LENGTH_SHORT).show()
        }


    }


    private fun displayReviews(){
        doAsync {
            var reviews: ArrayList<Reviews> = ArrayList()
            var usersName: ArrayList<String?> = ArrayList()
            for (r in realmHelper.getBook(bookId)?.ListOfReviews!!)
            {
                reviews.add(Reviews(r.ReviewId,r.Content,r.Rating,r.AuthorId))
                usersName.add(r.AuthorId?.Username)
            }
            uiThread {
                var adapter =
                    ReviewsAdapter(
                        reviews,
                        usersName
                    )
                rv_reviews.adapter = adapter
                rv_reviews.layoutManager = LinearLayoutManager(baseContext)
            }
        }
    }

    private fun connectToRealm() {
        try{
            realmHelper = RealmHelper()
            realmHelper!!.connectToRealm(this)
            //realmHelper.addToGenres()
        }catch (e: Exception){
            Log.i("INFO", e.toString())
        }

    }
}