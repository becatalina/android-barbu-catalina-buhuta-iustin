package com.example.pocketbookshelf.realm.models


import io.realm.RealmObject
import io.realm.RealmList
import io.realm.annotations.PrimaryKey

open class Shelves()  : RealmObject() {

    @PrimaryKey
    var ShelfId: Long = 0
        get() = field

    var Title: String = ""
        get() = field

    var ListOfBooks: RealmList<Books>? = RealmList<Books>()
        get() = field

    var User: Users? = null
        get() = field

    constructor(i:Long, t:String, l:RealmList<Books>?, u:Users?) : this(){
        ShelfId = i
        Title = t
        ListOfBooks = l
        User = u
    }

}