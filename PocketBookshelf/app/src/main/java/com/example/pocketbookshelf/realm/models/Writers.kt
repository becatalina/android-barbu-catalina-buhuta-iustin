package com.example.pocketbookshelf.realm.models

import androidx.annotation.RequiresPermission
import io.realm.RealmObject
import java.util.Date
import io.realm.annotations.PrimaryKey

open class Writers() : RealmObject() {

    @PrimaryKey
    var WritersId: Long = 0
        get() = field

    var FirstName: String = ""
        get() = field

    var LastName: String = ""
        get() = field

    var Biography: String = ""
        get() = field

    var DateOfBirth: Date? = null
        get() = field

    var DateOfDeath: Date? = null
        get() = field

    var ImagePath: String? = null
        get() = field
        set

    constructor(i: Long, f:String, l:String, b:String, db:Date?, dd:Date?):this(){
        WritersId = i
        FirstName = f
        LastName = l
        Biography = b
        DateOfBirth = db
        DateOfDeath = dd
    }
}