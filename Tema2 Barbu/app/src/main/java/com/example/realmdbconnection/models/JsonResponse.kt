package com.example.realmdbconnection.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.json.JSONObject


@JsonIgnoreProperties(ignoreUnknown = true)
class JsonResponse(val name: String): JSONObject() {

}


