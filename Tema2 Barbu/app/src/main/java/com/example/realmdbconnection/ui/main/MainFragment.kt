package com.example.realmdbconnection.ui.main

import android.app.AlertDialog
import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.realmdbconnection.R
import com.example.realmdbconnection.RealmHelper
import com.example.realmdbconnection.adapters.PersonAdapter
import com.example.realmdbconnection.models.JsonResponse
import com.example.realmdbconnection.models.Person
import com.google.gson.Gson
import io.realm.Realm
import kotlinx.android.synthetic.main.main_fragment.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.URL


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainFragment : Fragment(){

    companion object {
        fun newInstance() = MainFragment()

    }

    var peopleList: ArrayList<String>? = null
    var realmHelper: RealmHelper? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        connectToRealm()
        //can do without it, but there will be too many people on the screen
        realmHelper?.delete()

        btnAdd.setOnClickListener {
            AddPerson(input_firstName.text.toString(), input_lastName.text.toString())
            input_firstName.setText("")
            input_lastName.setText("")
            displayListOfPeople()
        }

        btnRemove.setOnClickListener {
            val input = input_firstName.text.toString() + " "+ input_lastName.text.toString()
            if (!peopleList?.contains(input)!!){
                showNonExistentDialog(view)
                return@setOnClickListener
            }

            realmHelper?.remove(input)
            input_firstName.setText("")
            input_lastName.setText("")
            displayListOfPeople()
        }

        btnSync.setOnClickListener {
            doAsync {
                val url = URL("https://jsonplaceholder.typicode.com/users").readText()
                uiThread {
                    val people = Gson().fromJson(url, Array<JsonResponse>::class.java).toList()
                    for (person in people){
                        var nameSplit = person.name.split(" ", limit = 2)
                        AddPerson(nameSplit[0], nameSplit[1])
                    }
                    displayListOfPeople()
                }
            }
        }

        displayListOfPeople()
    }

    fun connectToRealm(){

        Realm.init(context!!)
        realmHelper = RealmHelper(Realm.getDefaultInstance())
    }

    fun displayListOfPeople(){
        peopleList = realmHelper?.retrieve()
        val layoutManager = LinearLayoutManager(context!!)
        val personAdapter = peopleList?.let { PersonAdapter(it) }
        person_recycler_view.layoutManager = layoutManager
        person_recycler_view.adapter = personAdapter
    }

    private fun showExistsDialog(view:View?){
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Invald Input")
        builder.setMessage("The name already exists.")
        builder.show()
    }

    private fun showNonExistentDialog(view:View?){
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Invald Input")
        builder.setMessage("The name does not exist. Maybe you swapped first name with last name")
        builder.show()
    }

    private fun AddPerson(firstName: String, lastName: String){
        val input = "$firstName $lastName"
        if (peopleList?.contains(input)!!){
            showExistsDialog(view)
            return
        }

        realmHelper?.save(Person((10..10000).random(),
            firstName, lastName
        ))
    }



}
